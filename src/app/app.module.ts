import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './pages/home/home.component';
import { RelatorioFormComponent } from './pages/relatorio-form/relatorio-form.component';
import { HeaderComponent } from './header/header.component';

import { MaterialModule } from './modules/material/material.module'

import {ReactiveFormsModule} from '@angular/forms'
import { FormsModule } from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BotaoNavegacaoComponent } from './componentes/botao-navegacao/botao-navegacao.component';
import { ListaEquipamentosComponent } from './pages/lista-equipamentos/lista-equipamentos.component';
import { LogoComponent } from './componentes/logo/logo.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RelatorioFormComponent,
    HomeComponent,
    RelatorioFormComponent,
    HeaderComponent,
    BotaoNavegacaoComponent,
    ListaEquipamentosComponent,
    LogoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
