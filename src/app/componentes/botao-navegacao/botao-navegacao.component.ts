import { Component, Input } from '@angular/core';

@Component({
  selector: 'botao-navegacao',
  templateUrl: './botao-navegacao.component.html',
  styleUrls: ['./botao-navegacao.component.css']
})
export class BotaoNavegacaoComponent {

  @Input()
  nome !: String;

  @Input()
  iconeTela !: String;

}
