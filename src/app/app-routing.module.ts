import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { RelatorioFormComponent } from './pages/relatorio-form/relatorio-form.component';
import { ListaEquipamentosComponent } from './pages/lista-equipamentos/lista-equipamentos.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'cadastroRelatorio',
    component: RelatorioFormComponent
  },
  {
    path: 'listaEquipamentos',
    component: ListaEquipamentosComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
