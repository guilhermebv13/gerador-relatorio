import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  url='localhost:8080/api';

  constructor(public http : HttpClient) { }

  enviarImagem(file:File) {
    this.http.post(this.url+'/enviarImagem', {file}).subscribe();
  }

  teste() {
    this.http.get(this.url+'/teste').subscribe(resp => {
      console.log(resp);
    });
  }

}