import { Component, Inject } from '@angular/core';

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-relatorio-form',
  templateUrl: './relatorio-form.component.html',
  styleUrls: ['./relatorio-form.component.css']
})
export class RelatorioFormComponent {

  relatorioForm !: FormGroup;

  inspecaoInterna : boolean = false;
  inspecaoExterna : boolean = false;

  valvulaForms : number[] = [];

  imageUrl: string | ArrayBuffer | null = null;

  constructor(public formBuilder : FormBuilder,
              private _adapter: DateAdapter<any>,
              @Inject(MAT_DATE_LOCALE) private _locale: string,) {
    this._locale = 'br';
    this._adapter.setLocale(this._locale);
    this.relatorioForm = this.criarForm();
  }

  alterarEstadoTipoInspecao(checked:any,interna : boolean) {
    if(interna) {

      this.inspecaoInterna = checked;
      if(checked) {
        this.relatorioForm.controls['descricaoInspecaoInterna'].setValidators([Validators.required]);
      }else{
        this.relatorioForm.controls['descricaoInspecaoInterna'].setValidators([]);
      }

    }else {

      this.inspecaoExterna = checked;
      if(checked) {
        this.relatorioForm.controls['descricaoInspecaoExterna'].setValidators([Validators.required]);
        this.relatorioForm.controls['descricaoInspecaoExterna'].updateValueAndValidity();
      }else{
        this.relatorioForm.controls['descricaoInspecaoExterna'].setValidators([]);
        this.relatorioForm.controls['descricaoInspecaoExterna'].updateValueAndValidity
      }

    }
  }

  atualizarNumValvulas() {
    this.valvulaForms = Array.from({ length: this.relatorioForm.controls['numValvulas'].value });
    
    let fabricanteValvulasArray = (this.relatorioForm.controls['fabricanteValvula'] as FormArray);
    let modeloValvulasArray = (this.relatorioForm.controls['modeloValvula'] as FormArray);
    let tagValvulasArray = (this.relatorioForm.controls['tagValvula'] as FormArray);
    let diametroValvulasArray = (this.relatorioForm.controls['diametroValvula'] as FormArray);
    let pressaoAjusteValvulasArray = (this.relatorioForm.controls['pressaoAjusteValvula'] as FormArray);
    let certificadoCalibracaoValvulasArray = (this.relatorioForm.controls['certificadoCalibracaoValvula'] as FormArray);
    let dataCalibracaoValvulasArray = (this.relatorioForm.controls['dataCalibracaoValvula'] as FormArray);
    let localInstalacaoValvulasArray = (this.relatorioForm.controls['localInstalacaoValvula'] as FormArray);

    if(fabricanteValvulasArray.length > this.relatorioForm.controls['numValvulas'].value)  {
      while(fabricanteValvulasArray.length > this.relatorioForm.controls['numValvulas'].value) {
        fabricanteValvulasArray.removeAt(fabricanteValvulasArray.length-1);
        modeloValvulasArray.removeAt(modeloValvulasArray.length-1);
        tagValvulasArray.removeAt(tagValvulasArray.length-1);
        diametroValvulasArray.removeAt(diametroValvulasArray.length-1);
        pressaoAjusteValvulasArray.removeAt(pressaoAjusteValvulasArray.length-1);
        certificadoCalibracaoValvulasArray.removeAt(certificadoCalibracaoValvulasArray.length-1);
        dataCalibracaoValvulasArray.removeAt(dataCalibracaoValvulasArray.length-1);
        localInstalacaoValvulasArray.removeAt(localInstalacaoValvulasArray.length-1);
      }
    }else {
      while(fabricanteValvulasArray.length < this.relatorioForm.controls['numValvulas'].value) {
        fabricanteValvulasArray.push(this.formBuilder.control(null,Validators.required));
        modeloValvulasArray.push(this.formBuilder.control(null,Validators.required));
        tagValvulasArray.push(this.formBuilder.control(null,Validators.required));
        diametroValvulasArray.push(this.formBuilder.control(null,Validators.required));
        pressaoAjusteValvulasArray.push(this.formBuilder.control(null,Validators.required));
        certificadoCalibracaoValvulasArray.push(this.formBuilder.control(null,Validators.required));
        dataCalibracaoValvulasArray.push(this.formBuilder.control(null,Validators.required));
        localInstalacaoValvulasArray.push(this.formBuilder.control(null,Validators.required));
      }
    }
    
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageUrl = e.target.result;
      };
      reader.readAsDataURL(file);
    }
  }

  criarForm() {
    return this.formBuilder.group({
      //CAPA
      numeroRelatorio: [null, Validators.required],
      numeroArtigo: [null, Validators.required],
      nomeCliente: [null, Validators.required],
      vaso: [null, Validators.required],
      dataInspecao: [null, Validators.required],
      numeroInventario: [null, Validators.required],

      //TIPOS DE INSPEÇÃO
      descricaoInspecaoInterna: [null],
      descricaoInspecaoExterna: [null],

      //DADOS GERAIS
      clienteProprietario: [null,Validators.required],
      cnpj: [null,Validators.required],
      inscricaoEstadual: [null,Validators.required],
      local: [null, Validators.required],
      cep: [null, Validators.required],
      cidade: [null, Validators.required],


      //DADOS DO EQUIPAMENTO
      tagEquipamento: [null, Validators.required],
      fabricanteEquipamento: [null, Validators.required],
      anoFabricacao: [null, Validators.required],
      codigoProjeto: [null, Validators.required],
      anoEdicao: [null, Validators.required],
      adenda: [null, Validators.required],
      pmta: [null, Validators.required],
      pressaoTesteHidrostatico: [null, Validators.required],
      fluidoTrabalho: [null, Validators.required],
      categoriaNR13: [null, Validators.required],
      
      //DISPOSITIVOS DE SEGURANÇA
      fabricanteValvula: this.formBuilder.array([]),
      modeloValvula: this.formBuilder.array([]),
      tagValvula: this.formBuilder.array([]),
      diametroValvula: this.formBuilder.array([]),
      pressaoAjusteValvula: this.formBuilder.array([]),
      certificadoCalibracaoValvula: this.formBuilder.array([]),
      dataCalibracaoValvula: this.formBuilder.array([]),
      localInstalacaoValvula: this.formBuilder.array([]),
      notaSeguranca: [null],

      numValvulas:[null, Validators.required], //CAMPO DE APOIO, NÃO DEVE SER ENVIADO PARA API

      //EXAME INTERNO E EXTERNO
      resultadoInspecaoInterna: [null],
      resultadoInspecaoExterna: [null],
      basesSuporte: [null],
      

      //RESULTADO DAS MEDIÇÕES
      corpo: [null,Validators.required],
      tampoEsquerdo: [null, Validators.required],
      tampoDireito: [null, Validators.required]
    });
  }

}