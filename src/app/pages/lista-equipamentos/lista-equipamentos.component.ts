import { Component } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: number;
}

const ELEMENT_DATA: Relatorio[] = [
  {numItem: 1, numRelatorio: '2342513', equipamento: 'vaso 1', tag: 'COMP-001', numSerie: '321-52344', fabricante: 'fab1', localizacao : 'Manutenção', dataExecucao: new Date().toLocaleDateString(), vencimentoExterno: new Date().toLocaleDateString(), vencimentoInterno: new Date().toLocaleDateString(), idRelatorio : 1},
  {numItem: 2, numRelatorio: '0743214', equipamento: 'vaso 2', tag: 'COMP-002', numSerie: '421-44433', fabricante: 'fab2', localizacao : 'Produção', dataExecucao: new Date().toLocaleDateString(), vencimentoExterno: new Date().toLocaleDateString(), vencimentoInterno: new Date().toLocaleDateString(), idRelatorio : 2},
  {numItem: 3, numRelatorio: '6528210', equipamento: 'vaso 3', tag: 'COMP-003', numSerie: '123-12345', fabricante: 'fab3', localizacao : 'Suporte', dataExecucao: new Date().toLocaleDateString(), vencimentoExterno: new Date().toLocaleDateString(), vencimentoInterno: new Date().toLocaleDateString(), idRelatorio : 3},
  {numItem: 4, numRelatorio: '3561392', equipamento: 'vaso 4', tag: 'COMP-004', numSerie: '634-63290', fabricante: 'fab4', localizacao : 'Refinaria', dataExecucao: new Date().toLocaleDateString(), vencimentoExterno: new Date().toLocaleDateString(), vencimentoInterno: new Date().toLocaleDateString(), idRelatorio : 4},
];

@Component({
  selector: 'app-lista-equipamentos',
  templateUrl: './lista-equipamentos.component.html',
  styleUrls: ['./lista-equipamentos.component.css']
})  
export class ListaEquipamentosComponent {
  displayedColumns: string[] = ['numItem', 'numRelatorio', 'equipamento', 'tag', 'numSerie', 'fabricante', 'localizacao', 'dataExecucao', 'vencimentoExterno', 'vencimentoInterno', 'idArquivo'];
  dataSource = ELEMENT_DATA;

  baixarRelatorio(idRelatorio : number){
    
  }

}

export interface Relatorio {
  numItem : number;
  numRelatorio : String;
  equipamento : String;
  tag : String;
  numSerie : String;
  fabricante : String;
  localizacao : String;
  dataExecucao : String;
  vencimentoExterno : String;
  vencimentoInterno : String;
  idRelatorio : number;
}